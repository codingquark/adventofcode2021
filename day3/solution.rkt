#lang racket

(require advent-of-code
         threading
         awesome-list)

(define input-numbers
  (~> (open-aoc-input (find-session) 2021 3 #:cache #t)
      ;; (open-input-file "./example.txt")
      (in-port read-line _)
      (sequence->list)
      (map (lambda (arg)
             (string-split arg #rx"(?<=.)(?=.)"))
           _)))

(define transposed-numbers (transpose input-numbers))

(define (most-frequent-in-hash freq-hash-table)
  (cond [(equal? (hash-count freq-hash-table) 1)
         (string? (hash-keys freq-hash-table))
         (car (hash-keys freq-hash-table))]
        [(equal? (hash-ref freq-hash-table "1")
                 (hash-ref freq-hash-table "0"))
         "1"]
        [(> (hash-ref freq-hash-table "1")
            (hash-ref freq-hash-table "0"))
         "1"]
        [else "0"]))

(define (least-frequent-in-hash freq-hash-table)
  (cond [(equal? (hash-count freq-hash-table) 1)
         (string? (hash-keys freq-hash-table))
         (car (hash-keys freq-hash-table))]
        [(equal? (hash-ref freq-hash-table "1")
                 (hash-ref freq-hash-table "0"))
         "0"]
        [(> (hash-ref freq-hash-table "1")
            (hash-ref freq-hash-table "0"))
         "0"]
        [else "1"]))

(define power-consumption
  (for/fold ([gamma-rate-binary ""]
             [epsilon-rate-binary ""]
             #:result (list gamma-rate-binary epsilon-rate-binary))
            ([row (in-list transposed-numbers)])
    (let ([freq (frequencies row)])
      (if (equal? (most-frequent-in-hash freq) "0")
          (values
           (string-append gamma-rate-binary "0")
           (string-append epsilon-rate-binary "1"))
          (values
           (string-append gamma-rate-binary "1")
           (string-append epsilon-rate-binary "0"))))))

(* (string->number (car power-consumption) 2)
   (string->number (cadr power-consumption) 2))

(define oxygen-generator-rating
  (for/fold ([input input-numbers]
             [index 0]
             #:result (car input))
            ([i (in-list (range (length transposed-numbers)))])
    (let ([most-frequent
           (most-frequent-in-hash (frequencies (list-ref (transpose input) index)))])
      (if (equal? (length input) 1)
          (values input index)
          (values
           (filter
            (lambda (arg)
              (equal? most-frequent (list-ref arg index)))
            input)
           (add1 index))))))

(define co2-scrubber-rating
  (for/fold ([input input-numbers]
             [index 0]
             #:result (car input))
            ([i (in-list (range (length transposed-numbers)))])
    (let ([least-frequent
           (least-frequent-in-hash
            (frequencies (list-ref (transpose input) index)))])
      (if (equal? (length input) 1)
          (values input index)
          (values
           (filter
            (lambda (arg)
              (equal? least-frequent (list-ref arg index)))
            input)
           (add1 index))))))

(* (string->number (string-join oxygen-generator-rating "") 2)
   (string->number (string-join co2-scrubber-rating "") 2))
