#lang racket

(require advent-of-code
         threading
         awesome-list
         )

(define board-rows 5)
(define board-cols 5)

(define input-data
  (file->lines "input.txt"))

(define numbers-drawn
  (map string->number (string-split (car input-data) ",")))

(define boards-data
  (filter-not empty? (map string-split (cdr input-data))))

(define boards
  (for/fold ([boards '()]
             [lines boards-data]
             #:result boards)
            ([index (range (/ (length boards-data) board-rows))])
    (values (append
             boards
             (list (map
                    (lambda (line)
                      (map (lambda (element)
                             (list (string->number element) 0))
                           line))
                    (take lines 5))))
            (drop lines board-rows))))

(define (in-board? board element)
  (for/list ([row (in-list board)])
    (for/list ([cell (in-list row)])
      (if (equal? (car cell) element)
          #t
          #f))))

(define (update-board board number-drawn)
  (for/fold ([updated-board null])
            ([row (in-list board)])
    (define new-row
      (for/fold ([new-row '()])
                ([cell (in-list row)])
        (cond [(equal? (car cell) number-drawn)
               (if (empty? new-row)
                   (list (list number-drawn 1))
                   (append new-row (list (list number-drawn 1))))]
              [else
               (if (empty? new-row)
                   (list cell)
                   (append new-row (list cell)))])))
    (if (empty? updated-board)
        (list new-row)
        (append updated-board (list new-row)))))

(define (is-row-marked? row)
  (if (equal? (apply + (map cadr row)) 5)
      #t
      #f))

(define (has-won? board)
  (if (or
       (member #t
               (for/list ([row (in-list board)])
                 (is-row-marked? row)))
       (member #t
               (for/list ([column (in-list (transpose board))])
                 (is-row-marked? column))))
      #t
      #f))

(define (unmarked-numbers board)
  (flatten (for/list ([row (in-list board)])
    (map car (filter (lambda (cell)
              (equal? (cadr cell) 0))
            row)))))

(define (calculate-score board n)
  (* (apply + (unmarked-numbers board))
     n))

(for/fold ([marked-boards boards]
           [winning-board '()]
           [score 0]
           [previous-n null])
          ([n (in-list numbers-drawn)])
  (display "Remaining boards: ")
  (displayln (length marked-boards))
  (for/fold ([played-boards '()]
             [winning-board '()]
             [score 0]
             [previous-n null])
            ([board (in-list marked-boards)])

    (define updated-board
      (update-board board n))
    (define board-status (has-won? updated-board))

    (cond [board-status
           (display "Winning board: ")
           (displayln updated-board)
           (displayln (calculate-score updated-board n))])           

    (if (empty? played-boards)
        (values
         (if (not board-status)
             (list updated-board)
             '())         
         (if (and board-status
                  (empty? winning-board))
             updated-board
             '())
         0
         n)
        (values
         (if (not board-status)
             (append played-boards (list updated-board))
             played-boards)         
         (if (and board-status
                  (empty? winning-board))
             updated-board
             '())
         0
         n))))
