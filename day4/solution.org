* Back story
  We are 1.5km deep in the ocean. It says there is no sunlight this
  deep and I can [[https://oceanservice.noaa.gov/facts/light_travel.html][confirm]]! Fascinating! At this depth, there are indeed
  squids in the ocean in real world.

  We are so deep that we are going to play games with squids - a
  probable reference to [[https://en.wikipedia.org/wiki/Squid_Game][Squid Game]]. The game will be of bingo. The
  ship has bingo to keep passengers entertained. Are submarines even
  built for passengers? May be they just meant it’s for the crew?

  Since we are assuming the squid indeed wants to play bingo, we fire
  up the system and, well, play bingo. /I/ think we are deciding to
  play a game of bingo because we have been doing all these
  calculations for 3 days now want to waste time. It’s ruse!

* Puzzle
  The submarine’s bingo /subsystem/ generates:
  1. Random order in which to draw numbers
  2. Random set of boards

  First line of the input is the order in which to draw the
  numbers. Then one blank lines and then one board row of 5 numbers
  per line to create a 5x5 grid. After each grid there’s blank line.

  We mark each number in each grid as we draw them. The board that
  first manages to mark one entire row /or/ column wins.

  To calculate the score
  1. Sum the unmarked numbers of the winning board
  2. Multiply the sum with the last number drawn

  Let’s see how we can store the data. First, we will create a list of
  drawn numbers like =(7 4 9 5 11 7 ...)=. To store the boards, we
  will have each board represented as list of lists with some
  additional data slots: =(((22 0) (13 0) (17 0) (11 0) (0 0)) ((8 0)
  (2 0) (23 0) (4 0) (24 0)) ...)=. Notice how each number of the
  board is also converted into a list. The second position is where we
  will store mark state - =0= for unmarked to start with and =1= when
  marked.

  #+begin_src racket
  #+end_src

  After a number is drawn, we mark first board, check if it wins. If
  the board does not win, we mark the next one and so on.

  This time, we are doing part 1 & 2 together. I will write what it
  does later, it’s weekend and I have errands to run. Check
  =solution.rkt= meanwhile. It is some of the worst crap I’ve written
  and I may never get to refactor it.
