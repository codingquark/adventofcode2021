* Back story
  Since Elves are not real, they are going to do *wrong* things!

  Why does a sleigh have keys? I cannot imagine such a thing and
  hence, the whole premise is false and I am out of the competition!

  I have never been into a submarine. I don’t know what it feels like
  so I will assume it feels like what they show in movies. From inside
  the submarine, get this, I am writing computer programs! Last year
  it was coin-gathering BTW. This year it is no more about fake money,
  but stars. Why stars? What is a star? How can I gather large nuclear
  fusion performing astronomical bodies?

* Puzzle
** Part 1
   The submarine is searching for the keys. To start the search, we are
   going to map the ocean floor. Quite ambitious! I worked with
   ultrasonic sensor [[https://codingquark.com/automation/2021/11/22/motor-automation.html][recently]] and it was fun.

   After the sonar sweep, we see the readings on a /small screen/ in
   form of a graph. Now, wait right here. Do you understand that the
   actual puzzle is probably going to be thousands of lines which
   implies the me in the submarine fed thousands of lines from a /small
   screen/ to - presumably - Emacs? Rest of the puzzle definition is
   just details. We need to find out many times the depth /increases/
   as the sonar scans.

   Okay, here is [[file:example.txt][example]] input. We are going to read the input
   file, convert lines to numbers and store it as a list.

   #+NAME: depths
   #+begin_src racket :var filename="example1.txt"
     (define depths
       (for/list ([line (file->lines filename)])
	 (string->number line)))

     (for/list ([line (file->lines filename)])
	 (string->number line))
   #+end_src

   #+RESULTS: depths
   : (199 200 208 210 200 207 240 269 260 263)

   Next, we need to iterate over these numbers and see if the current
   one is larger than the previous one. If so, increase a counter.

   #+begin_src racket :var filename="input1.txt"
     (define depths
       (for/list ([line (file->lines filename)])
	 (string->number line)))

     (define (count-increases depths)
       (for/fold ([counter 0]
		  [previous-number (car depths)]
		  #:result counter)
		 ([n (in-list (cdr depths))])
	 (values (if (< previous-number n)
		     (add1 counter)
		     counter)
		 n)))

     (count-increases depths)
   #+end_src

   #+RESULTS:
   : 1266
** Part 2
   This requires a sliding window. We are going to create a window of
   size 3, and compare sums of the window’s elements instead of going
   one-by-one.

   Using the same example from part 1, we will change the
   ~count-increases~ procedure to calculate in sliding windows.

   We can use the procedure defined for part 1 ~count-increases~ if we
   can pass a list of sums of each window. To do this, we are going to
   take next 3 elements of the list, sum them up and store them under
   another list. This list is then passed to ~count-increases~
   procedure. This process itself can be defined as a different named
   procedure, but there is no need to do that. Instead, we can pass
   the entire procedure as argument and it will be evaluated the
   normal way.

   Let us take a detailed look at it. What do we want? We want a
   /list/ containing sums of 3 elements-wide sliding window. Since we
   have to iterate over elements one-by-one, we will create a
   /temporary list/ called ~window~.

   First time, the window will be ~null~. We will ~cons~ (that is,
   append) the element ~n~ to ~window~ and take the next iteration. In
   2 more iterations, the ~length~ of ~window~ will be 3. When this
   happens, we will sum the elements of the window up, and append the
   sum in ~sum~. Then, we can ~take~ last 2 elements of the window,
   append to ~n~ and we have the next sliding window prepared. This
   way, all the following iterations are going to be the next sliding
   window (of size 3), we will sum the same way and append the result
   to ~sum~. Such a list is then sent to ~count-increases~ and we get
   the answer!

   #+begin_src racket :var filename="input2.txt"
     (define depths
       (for/list ([line (file->lines filename)])
	 (string->number line)))

     (define (count-increases depths)
       (for/fold ([counter 0]
		  [previous-number (car depths)]
		  #:result counter)
		 ([n (in-list (cdr depths))])
	 (values (if (< previous-number n)
		     (add1 counter)
		     counter)
		 n)))

     (count-increases
      (for/fold ([sums null]
		 [window null]
		 #:result (reverse (cons (apply + window) sums)))
		([n (in-list depths)])
	(if (= (length window) 3)
	    (values (cons (apply + window) sums) (cons n (take window 2)))
	    (values sums (cons n window)))))
   #+end_src

   #+RESULTS:
   : 1217
