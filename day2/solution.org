* Back story
  I am listening to Kraftwerk mix.

  This puzzle wants me to find out how to /pilot/ a submarine. Let’s
  be realistic, you cannot figure out how to pilot a sumarine by
  writing program. You can send probes in outer spaces, but you cannot
  learn to pilot a spaceship by writing Racket programs, can you?

  Given the range of commands this /bird/ can take (are subs called
  birds? Fishes?), I can surely learn to make it move. Here is what it
  can do:
  1. ~forward~
  2. ~down~
  3. ~up~

  Yep, that’s all. And the submarine already has planned a
  course. Since it does not say “submarine’s on-board AI”, I will give
  it a pass.

* Puzzle
** Part 1
   We are going to get a list of commands that the submarine has
   prepared, and find out where it will end up after all the commands
   have been executed.

   Some “gotchas” that really just simple. When the command says ~down
   5~ what it really means is that we want to /increase/ the depth by
   5 - so the submarine goes /up/. If it were to go down, the depth
   would decrease.

   Alright, let’s setup our input. We will read by line, split the line
   by ~space~ character and create a pair for each command -
   ~("forward" 8)~ for example. Resulting structure will be a list of
   such pairs.

   #+begin_src racket :var filename="/Users/codingquark/workspace/adventofcode2021/day2/example.txt" :eval no :tangle yes
     #lang racket

     (require advent-of-code
	      threading)

     (define input-commands
       (~> (open-aoc-input (find-session) 2021 2 #:cache #t)
	   ;; (open-input-file filename)
	   (in-port read-line _)
	   (sequence->list _)
	   (map string-split _)))
   #+end_src

   Now that we have our commands in an appropriate data structure, we
   are simply going to loop over list of lists, and calculate the sum
   of ~forward~ commands. Then, we calculate ~depth~ by iterating and
   multiply the resulting values

   #+begin_src racket :tangle yes
     (define (calculate-total-distance)
       (for/sum ([command (in-list input-commands)])
	 (if (equal? (car command) "forward")
	     (string->number (cadr command))
	     0)))

     (define (calculate-total-depth)
       (for/sum ([command (in-list input-commands)])
	 (cond [(equal? (car command) "down") (string->number (cadr command))]
	       [(equal? (car command) "up") (- 0 (string->number (cadr command)))]
	       [else 0])))

     (* (calculate-total-distance) (calculate-total-depth))
   #+end_src

** Part 2
   Okay, part 1 taught me quite a bit about various functions of
   Racket. I rewrote the code multiple times. I think I am getting a
   better grip. One rewrite was because I would’ve had to use ~set~
   equivalent and I do /not/ want to do that.

   It says “based on your calculations, the planned course doesn’t
   seem to make any sense”. I wonder why. Let’s see, horizontal
   distance traveled was 1911 & depth was 779. Sure, still does not
   make sense but I don’t know to make sense of any other numbers!

   Turns out, the /submarine manual/ specifies the process is
   /slightly more complicated/. Of course, this clearly means that in
   the first part I just /went with it/ without consulting the
   manual. What a barbaric approach.

   This slightly more complicated process adds a new value to track:
   ~aim~.
   1. ~down X~ increase ~aim~ by X units
   2. ~up X~ decreases ~aim~ by X units
   3. ~forward X~
      1. Increases horizontal position by X units
      2. Increases depth by ~aim * X~

   Hmm, when forward changes the value of depth, shouldn’t aim change
   in turn as well? The wording is clear though, it changes /depth/,
   and does not talk about down/up.

   Like before, let’s open up our example input.

   #+begin_src racket :var filename="/Users/codingquark/workspace/adventofcode2021/day2/example.txt" :eval no :tangle no
     #lang racket

     (require advent-of-code
	      threading)

     (define input-commands
       (~> (open-input-file filename)
	   ;; (open-aoc-input (find-session) 2021 2 #:cache #t)
	   (in-port read-line _)
	   (sequence->list _)
	   (map string-split _)))
   #+end_src

   Now, let’s update the previous process, to calculate aim and
   related updates. In part 1 we calculated depth and distance in
   different functions. But here, depth and distance /both/ update the
   value of aim. The horizontal distance calculation remains the
   same. To calculate depth, we need to take care of aim.

   I could not find any way to keep ~aim~ maintained without using
   ~set~, and avoiding recursion. Lisps love recursion, but for loops
   are nice.

   So, I *stole* the following from a fellow Racketeer. Let’s see how
   it works. In the simplest of terms, ~for/fold~ (full clarification,
   that is the approach I was messing with when I saw the solution I
   could steal). We start with initial values of ~aim~, ~depth~,
   ~distance~ set to ~0~. ~for/fold~ keeps an accumulator for these
   three values. Then, in each iteration, we pass the updated values
   of the three parameters for the given direction. It’s just like
   recursion. Keep calling the function itself but with updated
   values. The ~for/fold~ form makes everything look nice and tidy. We
   then use Racket magic of ~#:result~ to multiply distance with depth
   and that’s our answer. So simple. I would update Part 1 to use
   this, but no, this shows I learned.

   #+begin_src racket :var filename="/Users/codingquark/workspace/adventofcode2021/day2/example.txt"
     #lang racket

     (require advent-of-code
	      threading)

     (define input-commands
       (~> ;; (open-input-file filename)
	   (open-aoc-input (find-session) 2021 2 #:cache #t)
	   (in-port read-line _)
	   (sequence->list _)
	   (map string-split _)))

     (for/fold ([aim 0]
		[depth 0]
		[distance 0]
		#:result (* depth distance))
	       ([command (in-list input-commands)])
       (match command
	 [(list "forward" x)
	  (values aim
		  (+ depth (* aim (string->number x)))
		  (+ distance (string->number x)))]
	 [(list "up" x)
	  (values (- aim (string->number x))
		  depth
		  distance)]
	 [(list "down" x)
	  (values (+ aim (string->number x))
		  depth
		  distance)]))
   #+end_src

   #+RESULTS:
   : 1176514794
